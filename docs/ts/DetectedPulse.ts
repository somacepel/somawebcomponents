interface Serializable {
}
module imadp {
  export class DetectedPulse implements Serializable {
    private cycleId: Number;
    // MICROSSEGUNDOS
    private timestamp: Number;
    private phaseIndex: Number;
    // FIXME: To float
    private peakValue: Number;
    private pulseClass: Number;

    public constructor(cycleId?: Number, timestamp?: Number, phaseIndex?: Number, peakValue?: Number, pulseClass?: Number) {
      this.cycleId = cycleId || -1;
      this.timestamp = timestamp || 0;
      this.phaseIndex = phaseIndex;
      this.peakValue = peakValue;
      this.pulseClass = pulseClass;
    }
    public getPulseClass(): Number {
      let x: Uint16Array
      return this.pulseClass;
    }

    public setPulseClass(pulseClass) {
      this.pulseClass = pulseClass;
    }

    public getCycleId(): Number {
      return this.cycleId;
    }

    public setCycleId(cycleId) {
      this.cycleId = this.cycleId;
    }

    public getTimestamp(): Number {
      return this.timestamp;
    }

    public setTimestamp(timestamp) {
      this.timestamp = timestamp;
    }

    public getPhaseIndex(): Number {
      return this.phaseIndex;
    }

    public setPhaseIndex(phaseIndex) {
      this.phaseIndex = this.phaseIndex;
    }

    public getPeakValue(): Number {
      return this.peakValue;
    }

    public setPeakValue(peakValue) {
      this.peakValue = peakValue;
    }

    public toString(): String {
      return "DetectedPulse [cycleId=" + this.cycleId + ", timestamp=" + this.timestamp + ", phaseIndex=" + this.phaseIndex
        + ", peakValue=" + this.peakValue + ", pulseClass=" + this.pulseClass + "]";
    }
  }
}
