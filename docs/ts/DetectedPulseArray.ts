package br.cepel.common.meassurement.imadp;

import java.io.Serializable;
import java.util.Arrays;

public class DetectedPulseArray implements Serializable {
	
	private static final long serialVersionUID = 1629160003193434020L;
	
	private DetectedPulse[] detectedPulses;
	
	public DetectedPulseArray() {
		super();
	}
	
	public DetectedPulseArray(DetectedPulse[] detectedPulses) {
		super();
		this.detectedPulses = detectedPulses;
	}

	public DetectedPulse[] getDetectedPulses() {
		return detectedPulses;
	}

	public void setDetectedPulses(DetectedPulse[] detectedPulses) {
		this.detectedPulses = detectedPulses;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "DetectedPulseArray [detectedPulses="
				+ (detectedPulses != null
						? Arrays.asList(detectedPulses).subList(0, Math.min(detectedPulses.length, maxLen)) : null)
				+ "]";
	}
}
