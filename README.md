# somaWebComponents

Este projeto somaWebComponents mantém as instruções para criar __componentes web__ baseados no Polymer (versão 1.0 ou superior) do SOMA

Indicamos, como material adicional, os vídeos desse playlist do Polycast: [https://www.youtube.com/playlist?list=PLOU2XLYxmsII5c3Mgw6fNYCzaWrsM3sMN](https://www.youtube.com/playlist?list=PLOU2XLYxmsII5c3Mgw6fNYCzaWrsM3sMN)

Usamos o Yeoman para criar os componentes reusáveis como descrito na documentação do Polymer em [https://www.polymer-project.org/1.0/docs/start/reusableelements.html](https://www.polymer-project.org/1.0/docs/start/reusableelements.html).

Assumimos aqui que o __Node JS__ e o __GIT__ já estão instalados no sistema.

## Passo a passo inicial

Instale o Bower

    sudo npm install -g bower

Depois instale o polyserve

    sudo npm install -g polyserve

Clone este repositório assim:

    git clone https://SEU_USUARIO_BITBUCKET@bitbucket.org/somacepel/somawebcomponents.git somaWebComponents

Instale o plugin EditorConfig no __Sublime Text 3__ usando o _Package Control_.

Depois faça o restart do Sublime.

No Terminal execute o download das dependencias do componente desejado e em seguida inicie o polyserve. No exemplo abaixo faço isso com o componente seed-element

    cd components/seed-element
    bower install
    polyserve

Abra a página do componente em : [http://localhost:8080/components/seed-element/index.html](http://localhost:8080/components/seed-element/index.html)

Veja a imagem abaixo:

![Componente seed-element](https://bytebucket.org/somacepel/somawebcomponents/raw/b3ab0879ff937fe1951ad368a3eb71a2190729ef/docs/img/seed-element_site.png?token=d6f58c0573b02114759db052f1a6b924a14d3dd7)

1. URL do Componente WEB
2. Link para Docuemntação do Componente WEB
3. Componentes WEB com mais de uma TAG podem ser vistos aqui.
4. Link para a Demonstração das funcionalidades do Componente WEB.

Até aqui nós criamos um componente e que funcionará como template para outros componentes e estamos apto a modificá-lo e testá-lo.

Uma breve investigação nos arquivos `bower.json` vemos que o que está no diretório seed-element descreve as seguintes Dependências:

    "dependencies": {
      "polymer": "Polymer/polymer#^1.0.0"
    },
    "devDependencies": {
      "iron-component-page": "PolymerElements/iron-component-page#^1.0.0",
      "web-component-tester": "*"
    }

Ou seja, apenas o Polymer, o iron-component-page e o web-component-tester.

Mas se observarmos o arquivo `bower.json` em `components/seed-element/bower_components/iron-component-page` podemos ver várias outras dependências.

Todos os componentes da cadeia de dependências são baixadas automaticamente quando executamos o comando `bower install` mostrado acima.

Veja também que o `polymer` está sob o Namespace `Polymer` e `iron-component-page` está sob o Namespace `PolymerElements`. isto é devido ao fato de estarem em repositórios Github diferentes, [https://github.com/Polymer/polymer](https://github.com/Polymer/polymer) e [https://github.com/PolymerElements/iron-component-page](https://github.com/PolymerElements/iron-component-page) respectivamente.


## Criando uma estrutura para WebComponents

Veja abaixo uma estrutura básica simples para uma __Aplicação Polymer__

![Estrutura da Aplicação](https://bytebucket.org/somacepel/somawebcomponents/raw/2eaf15286adc8bcab516bc29b5a0f40561f8e9df/docs/img/polymer-app.png?token=34e7ba87c2dbe168839ea98218d466ec5aa0a7d3)

No diretório da aplicação temos:

1. Arquivo index.html como entrypoint principal
2. O arquivo `bower.json` definindo as dependências da aplicação
3. O diretório bower_components criado automaticamente pelo Bower
4. A implementação de um elemento arbitrário chamado `my-element`
5. A implementação de uma das dependências do elemento citado.

O que desejamos é que tanto `my-element` quanto `my-dependency` tenham seu proprio projeto no GIT e que possam ser testados e documentados independentemente. 

Este é o propósito deste repositório, ou seja, manter um esqueleto do qual possamos copiar e gerar outros componentes, mantendo a documentação e os testes independentes e coesos.

O problema reside no fato do componente em si ter a sua propria estrutura de diretório como mostrado abaixo:

![Estrutura para Web Component](https://bytebucket.org/somacepel/somawebcomponents/raw/1bd7fc5475cea6f25815a12a2d4d3fcce85fdfb9/docs/img/polymer-web-component.png?token=d44adcb86c67b4ea0e21a91b389a7ff3243147fb)

1. Implementação do `my-element`
2. O arquivo `bower.json` definindo as dependências do componente WEB
3. O diretório bower_components criado automaticamente pelo Bower
4. A implementação do elemento do qual `my-element` depende. Assume-se que este já foi testado anteriormente e que já está disponível via Bower.

As duas estruturas são incompatíveis e o proposito do utilitário __polyserve__ é de compatibilizar isso supondo que todos os Componentes WEB são irmãos independente do nível de depedência de forma que possamos sempre escrever algo assim:

    <link rel="import" href="../my-dependency/my-dependency.html">

Ou seja `my-element.html` pode importar `my-dependency.html` fazendo referencia ao __diretório pai__ e isso para todo e qualquer componente. No Polymer isto é chamado __Canonical Path__ 

Por exemplo na implementação de `<iron-icon>` encontramos os seguintes Imports:

    <link rel="import" href="../polymer/polymer.html">
    <link rel="import" href="../iron-meta/iron-meta.html">
    <link rel="import" href="../iron-flex-layout/iron-flex-layout.html">

No arquivo `bower.json` deste componente encontramos a definição de dependências abaixo:

    "dependencies": {
      "iron-flex-layout": "polymerelements/iron-flex-layout#^1.0.0",
      "iron-meta": "polymerelements/iron-meta#^1.0.0",
      "polymer": "Polymer/polymer#^1.0.0"
    }

Podemos usar o Yeoman para gerar o esqueleto de um outro elemento à partir do `seed-element`

Criamos uma shell em `~/bin/yo-p-seed` com o seguinte conteúdo:

    cd ~/Desktop/Development/somaWebComponents
    cd components
    echo "Estou em `pwd` e vou gerar o componente $1 "
    mkdir -p $1  && cd $_
    yo polymer:seed $1

Depois executamos:

    chmod a+rx   ~/bin/yo-p-seed

Então podemos criar nosso WEB Components assim:

    yo-p-seed meu-componente

O Yeoman questiona o nome do usuário do Github e se desejamos usar o WCT.

A seguir uma interação típica

![Yeoman criando seed](https://bytebucket.org/somacepel/somawebcomponents/raw/0bed50a3c3e9adde9a57915d6dbe3715b3ea0f8b/docs/img/yeoman-seed.png?token=4215d066823cc6c390a8859556ac1ab1a6582eb6)

1. Informamos o nome do usuário no Github
2. Informamos o desejo de usar o WCT
3. O Yeoman gera os arquivos necessários
4. O Yeoman invoca o Bower para instalar as dependências.


Desta forma nossa estrutura de diretório ficará assim:

    .gitignore
    components
      meu-componente
        .gitignore
        .yo-rc.json
        bower.json
        demo
          index.html
        index.html
        meu-componente.html
        README.md
        test
          basic-test.html
          index.html
      seed-element
        .gitignore
        bower.json
        demo
          index.html
        hero.svg
        index.html
        README.md
        seed-element.html
        test
          basic-test.html
          index.html
      . . .
    docs
      . . . 
      img
        bower-json-yeoman.png
        polymer-app.png
        polymer-web-component.png
        seed-element_site.png
        yeoman-seed.png
        . . . 
    README.md

Veja agora como fica o nossos projeto no Sublime Text 3

![Detalhes do Bower json](https://bytebucket.org/somacepel/somawebcomponents/raw/0bed50a3c3e9adde9a57915d6dbe3715b3ea0f8b/docs/img/bower-json-yeoman.png?token=130d6383eae7e9bf46455d99430f5a3094762188)

1. Nosso projeto 
2. O arquivo .yo-rc.json registra o nome do usuário no Github
3. O arquivo bower.json mostra as dependências
4. Devemos alterar o autor do Componente
5. Devemos alterar a versão do Polymer para uma nais atual
6. Devemos alterar a versão do componente iron-component-page para uma nais atual


## Documentação

A documentação é criada via tag `<iron-component-page>`. Carregando o arquivo `index.html` podemos ver a documentação gerada:

    http://localhost:8080/components/meu-elemento/index.html

A ferramenta de docuemntação infere algumas informações sobre o componente à partir do código, mas também reconhece algums comentários no estilo JSDoc-style. 

Na docuemntação devemos:

* Sumarizar o funcionamento do componente e qual seu propósito
* Mostrar um exemplo do componente em ação.

A ferramenta por si só irá :

* Agrupar os atributos, métodos e eventos automaticamente 
* Criar o Link para o DEMO do componente.


## Publicando seu componente

Faça o __push__ do código para uma conta do GitHub e crie uma tag com o __release number__, assim outras pessoas podem instalar o componente usando o Bower.


## Pegadinhas

### Polymer Generator

O __Polymer Generator__ do __Yeoman__ gera uma Aplicação, um Elemento ou um Seed (este ultimo como descrito acima).

Para gerar Elemento ou Aplicação a abordagem é totalmente diferente dessa da qual falamos. 

No caso de gerar Elemento ou Aplicação devemos manter atualizado o arquivo `elements.html` e este é o único arquivo onde deve ser colocado todos os imports de todos os componentes usados na aplicação pois do contrário a `Vulcanização` pode falhar e o programa gerado não funcionará. Mas este não é o caso aqui nesteprojeto já que trabalhamos com a opção `seed` do _Polymer Generator_.

Se estiver interessado em ver o outro projeto que usa estes componentes e a outra opção do _Polymer Generator_ faça o clone de somaWebApp assim:

    git clone https://SEU_USUARIO_BITBUCKET@bitbucket.org/somacepel/somawebapp.git

> somaWebApp é o repositório da aplicação WEB do SOMA. Ele usa os componentes desenvolvidos e testados individualmente no projeto somaWebComponents e orquestra isso para prover a Aplicação final aos usuários.

### Referência temporária

No caso de um componente ainda não ter sido publicado no Bower e outro componente a ser desenvolvido dependa dele podemos criar um __Link Simbólico__ para disponibilizar o componente para o polyserve. Veja exemplo abaixo:

    cd bower_components
    ln -s ~/Desktop/Development/somaWebComponents/components/seed-element/ seed-element

Neste caso acima estou disponibiliando o seed-element para outro componente que dependa dele.

Este tipo de solução é temporária e após publicar o elemento no Bower devemos remover todo o diretório bower_components e executar `bower install`

### Acesso do Bower aos repositórios privados no Bitbucket

A __Referência temporária__ mostrada acima é uma solução frágil (sujeita a erros) e pode ser substituida pela descrita a seguir.

Adicione no arquivo `bower.json` o código abaixo mostrado na terceira linha:

    {
      "dependencies" : {
        "my_package_name" : "https://USER_NAME@bitbucket.org/USER_NAME/MY_REPO_NAME.git",
        "polymer": "Polymer/polymer#^1.0.0"
        . . . 
       }
    }

Por exemplo, consideremos o Componente WEB `app-globals` cuja definição está mostrada abaixo.

    <app-globals whathever-attr="something" debug></app-globals>

    The <i>whathever-attr</i> will be converted in app-globals immutable attribute and can be used by others Web Components on our Application

Considere agora o Componente WEB `app-login` que usa o Componente WEB `app-globals`. Este deve definir a dependêcia no seu arquivo `bower.json` da seguinte forma: `"app-globals" : "https://USER_NAME@bitbucket.org/somaelements/app-globals.git"`

Suponha o usuário parana então devemos colocar no arquivo bower.json o seguinte:

    "app-globals" : "https://parana@bitbucket.org/somaelements/app-globals.git"


Ao executarmos o comando de instalação de dependências do Bower vemos algo como mostrado abaixo:

![https://bytebucket.org/somacepel/somawebcomponents/raw/efa15a0f10813d8ed67fa92396cdcaf7bdf4ccdd/docs/img/bower-install-somaDesktopApp.png?token=2e6093c04bdbe47f6622d12810628ca00c4ed182](https://bytebucket.org/somacepel/somawebcomponents/raw/efa15a0f10813d8ed67fa92396cdcaf7bdf4ccdd/docs/img/bower-install-somaDesktopApp.png?token=2e6093c04bdbe47f6622d12810628ca00c4ed182)

1. invocando o comando
2. o Bower analiza as dependências
3. o Bower instala o componente


Como alternativa a HTTPS podemos usar o SSH e neste caso a URL tem outra estrutura. Veja abaixo:

    git@bitbucket.org:accountname/reponame.git 

Neste caso o sistema deve ter uma chave SSH autorizada.

#### Observação: 

Se houver problemas com a URL mostrada acima devido a bloqueio na porta 22 (porta padrão) veja a solução proposta em: [https://confluence.atlassian.com/bitbucket/use-the-ssh-protocol-with-bitbucket-221449711.html](https://confluence.atlassian.com/bitbucket/use-the-ssh-protocol-with-bitbucket-221449711.html) 

Nesta solução devemos usar a URL: `ssh://git@altssh.bitbucket.org:443/USER_NAME/MY_REPO_NAME/` 

### Criando Repositório para nosso componente

Escolhemos usar outro usuário no Bitbucket para criar os repositórios para cada um dos componentes. 
O login é `somaelements` e devemos criar repositórios como mostrado no exemplo abaixo:

![https://bytebucket.org/somacepel/somawebcomponents/raw/0998b9d7c3f9ba0aeb9c4ed09db9e24848b35ec1/docs/img/create-repository-WebComponent.png](https://bytebucket.org/somacepel/somawebcomponents/raw/0998b9d7c3f9ba0aeb9c4ed09db9e24848b35ec1/docs/img/create-repository-WebComponent.png)




### Criando chaves SSH 

    ssh -v
    ls -lAt ~/.ssh # Mostra hosts conhecidos (known_hosts) e chave publica RSA (se houver)
    ssh-keygen # Execute este comando caso não exista chave publica RSA definida
    cat ~/.ssh/id_rsa.pub # Verifique se a chave está OK
    pbcopy < ~/.ssh/id_rsa.pub # Copia a chave para o Clipboard do Sistema Operacional.

em https://bitbucket.org, escolha `avatar > Manage account`. O sistema vai exibir suas configurações de usuário.
Click em `SSH keys`. As `SSH Keys` existentes serão exibidas. Você pode então adicionar a chave publica criada e copiada para o Clipboard. Veja imagem abaixo:

![adicionando chave publica ssh no bitbucket](https://bytebucket.org/somacepel/somawebcomponents/raw/4cc922c5f323694c137a6ccff2bc6f127161e5bc/docs/img/chave-ssh-bitbucket.png?token=5327dc9857f64978ea78d28f0019e17786e7e395)

Após adicionar a sua chave publica na conta Bitbucket do usuário `somacepel`, todos os repositórios deste usuário (`somacepel`) estarão acessíveis, sem a necessidade de informar senha, à partir do host atual (onde foi executado o comando `ssh-keygen`) para o usuário atual (neste exemplo: `parana`).


## Uso de TypeScript para gerar JavaScript padrão ECMA Script 5

Consideremos o exemplo abaixo:

    @startuml
    class DetectedPulse  {
      + new(cycleId, timestamp, phaseIndex, peakValue, pulseClass);
      + int getPulseClass();
      + void setPulseClass(int pulseClass);
      + int getCycleId();
      + void setCycleId(int cycleId);
      + long getTimestamp();
      + void setTimestamp(long timestamp);
      + int getPhaseIndex();
      + void setPhaseIndex(int phaseIndex);
      + double getPeakValue();
      + void setPeakValue(double peakValue);
      + String toString();
    }
    DetectedPulseArray *--> DetectedPulse : detectedPulses
    Serializable <|. DetectedPulse
    DetectedPulseArray ..|> Serializable   
    @enduml


 ![dpa uml png](https://bytebucket.org/somacepel/somawebcomponents/raw/c95526e9299ef9ea4eff42288f354841a01688d5/docs/img/dpa-uml.png?token=83a3d2813a095719d897804e443e017a0235edff)   

 Podemos converter para TypeScript como mostrado abaixo:

    interface Serializable {
    }
    module imadp {
      export class DetectedPulse implements Serializable {
        private cycleId: Number;
        // MICROSSEGUNDOS
        private timestamp: Number;
        private phaseIndex:Number;
        // FIXME: To float
        private peakValue:Number;
        private pulseClass:Number;

        public constructor(cycleId: Number, timestamp:Number, phaseIndex:Number, peakValue:Number, pulseClass:Number) {
          this.cycleId = cycleId;
          this.timestamp = timestamp;
          this.phaseIndex = phaseIndex;
          this.peakValue = peakValue;
          this.pulseClass = pulseClass;
        }
        public getPulseClass(): Number {
          return this.pulseClass;
        }

        public setPulseClass(pulseClass) {
          this.pulseClass = pulseClass;
        }

        public getCycleId(): Number {
          return this.cycleId;
        }

        public setCycleId(cycleId) {
          this.cycleId = this.cycleId;
        }

        public getTimestamp(): Number {
          return this.timestamp;
        }

        public setTimestamp( timestamp) {
          this.timestamp = timestamp;
        }

        public getPhaseIndex(): Number {
          return this.phaseIndex;
        }

        public setPhaseIndex(phaseIndex) {
          this.phaseIndex = this.phaseIndex;
        }

        public getPeakValue(): Number {
          return this.peakValue;
        }

        public setPeakValue(peakValue) {
          this.peakValue = peakValue;
        }

        public toString(): String {
          return "DetectedPulse [cycleId=" + this.cycleId + ", timestamp=" + this.timestamp + ", phaseIndex=" + this.phaseIndex
            + ", peakValue=" + this.peakValue + ", pulseClass=" + this.pulseClass + "]";
        }
      } 
    }

Isso permitirá que acessemos o objeto da seguinte forma:

    let data: imadp.IMADPRecordData;
    // Aqui invocamos o Serviço para acesso remoto ao dado
    // .. Aqui entra algum código AJAX
    data = imaDPNS.model.data;
    let pulse: imadp.DetectedPulse;
    var pIdx = pulse.getPhaseIndex;
    // ...


## Resolução de Problemas

1. Você abre a página WEB e ela aparece em branco e na console do Desenvolvedor do Chrome aparece erros 404 para webcomponents-lite.js dentre outros arquivos. Solução: Posivelmente você não executou o comando `bower install` então o faça agora.

